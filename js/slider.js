$(function(){
  $('.slider__inner').slick({
    nextArrow: '<button type="button" class="slick-btn slick-next"><img src="images/right-arrow.png" class="slick-img"></button>',
    prevArrow: '<button type="button" class="slick-btn slick-prev"> <img src="images/left-arrow.png" class="slick-img"></button>',
     slidesToShow: 1,
     dots: true
  });
});
