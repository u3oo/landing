//scroll, count, with jquery logic
let  a = 0;
$(window).scroll(function() {

	let oTop = $('#counter').offset().top - window.innerHeight;
	if (a == 0 && $(window).scrollTop() > oTop) {
		$('.counter-value').each(function() {
			let $this = $(this);
			let  countTo = $this.attr('data-count');
			$({
				countNum: $this.text()
			}).animate({
				countNum: countTo
			},

			{
				duration: 3000,
				easing: 'swing',
				step: function() {
					$this.text(Math.floor(this.countNum));
				},
				complete: function() {
					$this.text(this.countNum);
					console.log('finished');
				}

			});
		});
		a = 1;
	}

});

//
//burger-menu
$(document).ready(function() {
	$('.header__btn-menu img').click(function() {
		$('ul').toggleClass('mobile__navbar');
	});
});



//modal
// function toggleModal() {
// 	const modal = document.getElementById('modal');

// 	if(modal.style.display == 'flex') {
// 		modal.style.display = 'none'
// 	} else {
// 		modal.style.display = 'flex'
// 	}
// }

function modalWindow(event) {
	let modal = document.getElementById('modalq');
	let content = document.getElementById('contentq');
	if(modal.style.display == 'flex') {
		modal.style.display = 'none';

	} else  {
		modal.style.display = 'flex'
		
	}
}
function handleContent(event){
	event.stopPropagation();
}
